import java.util.ArrayList;
import java.util.List;




public class Telecommande {
	
	private List<Objetactivable> objets;
	
	public Telecommande(){
		this.objets=new ArrayList<Objetactivable>();
		
	}
	
	public void ajouter(Objetactivable o){
		this.objets.add(o);
	}

	
	
	public void activer(int indice){
		if(indice < this.objets.size() && indice>=0){
			this.objets.get(indice).allumer();
		}
		
	}
	
	
	public void desactiver(int indice){
		if(indice < this.objets.size() && indice>=0){
			this.objets.get(indice).eteindre();
		}
	}
	
	public void activerTout(){
		for(int i=0;i<objets.size();i++){
			this.objets.get(i).allumer();
		}
	}
	
	public String toString(){
		String s="";
		for(int i=0;i<objets.size();i++){
			s=s+this.objets.get(i).toString()+"\n";
		}
		return s;
	}

}
