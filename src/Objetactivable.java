
public abstract class Objetactivable {
	
	/**
	 * nom de la lampe
	 */
	String nom;
	
	public Objetactivable(String paramNom){
		this.nom = paramNom;
	}
	
	

	public abstract void allumer() ;
	public abstract void eteindre() ;
	public abstract String toString() ;

}
