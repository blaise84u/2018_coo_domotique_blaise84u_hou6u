import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
* @author HOU Yunan
*
*/
public class TelecommandeTest {
	@Test
	public void TestConstructeur1() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");
		
		t.ajouter(l);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: Off\n");
	}
	@Test
	public void TestConstructeur2() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");
		
		t.ajouter(l);
		t.ajouter(l);
		t.ajouter(l);
		assertEquals (" ajout d'une lampe à une télécommande avec 1 élément ",t.toString(),"test: Off\ntest: Off\ntest: Off\n");
	}
	@Test
	public void TestActivation1() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");
		
		t.ajouter(l);
		t.activer(0);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: On\n");
	}
	@Test
	public void TestActivation2() {
		Telecommande t=new Telecommande();
		Lampe l=new Lampe("test");
		Lampe m=new Lampe("test2");
		t.ajouter(l);
		t.ajouter(m);
		t.activer(1);
		assertEquals ("ajout d'une lampe à une télécommande vide ",t.toString(),"test: Off\ntest2: On\n");
	}
	@Test
	public void TestActivation3() {
		Telecommande t=new Telecommande();
		t.activer(0);
	}
}
